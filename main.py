# this code should populate the elements part of the content.json of a h5p course presentation, which generally is where
# we got the most amount of work when bringing an pptx into this type of platform
# it must use the export from Microsoft PowerPoint pptx to PDF
# python 3.6 was used

#DEPENDENCIES: pptx-python, easygui

import easygui
import zipfile
import json
from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE_TYPE 
from lxml import etree
import re
from pathlib import Path
import os
from shutil import rmtree

rmtree("output/")

#temporary call
pptxFilePath = '/home/mamfbr/Documents/PPTXtoH5P/resources/pptxSample.pptx'

#call the system file browser to retrieve the pdf file
# pptxFilePath = easygui.fileopenbox(msg=None, title='Abrir PPTX', default='*', filetypes=".pptx", multiple=False)
# if pptxFilePath is not None:
# 	if re.search('\.pptx$', pptxFilePath):
# 		pptxFile = Presentation(pptxFilePath)
# 	else:
# 		easygui.msgbox('Bad File Extension!')
# 		print(pptxFilePath)
# else:
# 	easygui.msgbox('Bad File!')

#open the pptx file with Presentation so we can use all the pptx-python stuff
pptxFile = Presentation(pptxFilePath)


##PROGRAM FOLDER CREATION

#create output/ folder if it doesn't exist
if not os.path.exists('output/'):
	os.mkdir('output/')

#make the string with the partial path for later use, should be "output/*filename without .pptx*/"
partialPath = 'output/' + Path(pptxFilePath).stem + '/'

#create output/*name of pptx file*/ folder if it doesnt exist
if not os.path.exists(partialPath):
	os.mkdir(partialPath)

##END OF PROGRAM FOLDER CREATION

##BEGIN OF BLANK H5P CREATION

#unzip the blank H5P Presentation into the destination folder
zipReference = zipfile.ZipFile('resources/blankH5P.zip', 'r')
zipReference.extractall(partialPath)
zipReference.close()

#add the picture folder to *name*/content/images
if not os.path.exists(partialPath + 'content/images/'):
	os.mkdir(partialPath + 'content/images/')

##END OF BLANK H5P CREATION

###########################################################
#AFTER THIS POINT, WE CAN START MODIFYING THE H5P DIRECTLY#
##########################################################

#get content/content.json file in order to modify it
jsonInputFile = open('output/pptxSample/content/content.json', 'r')
#put into memory as a json object
jsonInput = json.load(jsonInputFile)
#close the file since we should be using the copy on memory pointed by jsonInput
jsonInputFile.close()

#iterate for every slide, searching for texts and pictures and then populating the pictures folder and json
#containing the H5P objects information

#counter for tracking which slide it is currently probing
slideCounter = 0
#count how many picures were extracted, for naming purposes (e.g. pic1.png, pic2.png...)
pictureCounter = 0

#create output/*name of file*/ folder
if not os.path.exists('output/' + Path(pptxFilePath).stem + '/pictures/'):
	os.mkdir('output/' + Path(pptxFilePath).stem + '/pictures/')


#list containing dictionaries, with keys value and name, value being the binary of the png file
#and name the picx.png, whereas x is the value of pictureCounter
shapeImageList = []

#iterate for every slide
for slide in pptxFile.slides:

	##create a folder for every slide
	# slideCounter += 1
	# if not os.path.exists('output/' + Path(pptxFilePath).stem + '/pictures/slide' + str(slideCounter) + '/'):
	# 	os.mkdir('output/' + Path(pptxFilePath).stem + '/pictures/slide' + str(slideCounter) + '/')	
	


	#iterate for every object in the slide
	for shape in slide.shapes:

		#test to see if the object is a picture
		if shape.shape_type == MSO_SHAPE_TYPE.PICTURE:

			#test to see if the image already exists
			duplicate = 0
			duplicateName = 'NULL'
			for picture in shapeImageList:
				#this comparison may prove itself to be quite onerous to the system, perhaps a hashing here would
				#give a better result if you see yourself having to batch hundreds of pptx
				if picture["value"] == shape.image.blob:
					duplicate = 1
					duplicateName = picture["name"]
					break
			#if there were no duplicates found, append to the list of images for latter usage/storage
			if duplicate == 0:
				shapeImageList.append({"value": shape.image.blob, "name": 'pic0' + str(pictureCounter).zfill(2)})
				#add to the count to the next picture will have a different name
				pictureCounter += 1
				#TODO: add to content.json slide
			else:
				pass
				#TODO: add duplicateName to content.json slide



		#test to see if the object is a text box
		if shape.shape_type == MSO_SHAPE_TYPE.TEXT_BOX:
			if not shape.has_text_frame:
				# print(shape.text)
				print('entrei')

			
		
		# shapeXml = shape.element

		# if not os.path.exists('output/' + Path(pptxFilePath).stem):
		# 	os.mkdir('output/' + Path(pptxFilePath).stem)
		# xmlfinal = open('output/'+ Path(pptxFilePath).stem + '/element' + str(slideCounter) + '.xml', 'w+')
		# xmlfinal.write(etree.tostring(shapeXml).decode('utf-8'))

for picture in shapeImageList:
	#print('output/'+Path(pptxFilePath).stem + '/pictures/' + picture["name"])
	open(partialPath + 'pictures/' + picture["name"], 'wb').write(picture["value"])