import json

jsonInputFile = open('output/pptxSample/content/content.json', 'r')
jsonInput = json.load(jsonInputFile)
jsonInputFile.close()
false = "false"
textSample ="""
{
	"action": {
	    "library": "H5P.AdvancedText 1.1",
	    "metadata": {
	        "authors": [],
	        "changes": [],
	        "contentType": "Text",
	        "extraTitle": "Sem título",
	        "license": "U",
	        "title": "Sem título"
	    },
	    "params": {
	        "text": "<h2><span style=\\"font-size:1.75em;\\">Segurança da Informação</span></h2>\\n"
	    },
	    "subContentId": "346f3e04-6978-4def-934a-4378789ea639"
	},
	"alwaysDisplayComments": false,
	"backgroundOpacity": 0,
	"buttonSize": "big",
	"displayAsButton": false,
	"goToSlideType": "specified",
	"height": 11.309523809524,
	"invisible": false,
	"solution": "",
	"width": 70.588235294118,
	"x": 16.59125188537,
	"y": 35.714285714286
}
"""
imagePath = "HAULAUDAHSIDAHSIUDHASUIDHASI"
imageSample ="""
{
    "action": {
        "library": "H5P.Image 1.0",
        "metadata": {
            "contentType": "Image",
            "license": "U"
        },
        "params": {
            "contentName": "Image",
            "file": {
                "copyright": {
                    "license": "U"
                },
                "height": 1623,
                "mime": "image/png",
                "path": """ + '\"' + imagePath + '\"' +""",
                "width": 873
            }
        },
        "subContentId": "34b26476-561f-4929-8908-8191d78f977c"
    },
    "alwaysDisplayComments": false,
    "backgroundOpacity": 0,
    "buttonSize": "big",
    "displayAsButton": false,
    "goToSlideType": "specified",
    "height": 96.428571428571,
    "invisible": false,
    "solution": "",
    "width": 26.395173453997,
    "x": 70.889894419306,
    "y": 2.9761904761905
}
"""

textSampleJson = json.loads(textSample)

jsonInput["presentation"]["slides"][0]["elements"].append(textSampleJson)

jsonOutputFile = open('output/pptxSample/content/content.json', "w+")

json.dump(jsonInput, jsonOutputFile)